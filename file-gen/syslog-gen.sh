#!/bin/bash
MESSAGES=("error syslog message" "debug syslog message" "warning syslog message" "info syslog message")
PRIORITIES=(0 1 2 3 4 5 6 7)
MSG_INTERVAL=1
COUNTPS=1
FILE_NAME=syslog.log

if [ $# -ne 1 ]; then
	echo "you must provide count of rows"
	exit 1;
fi

touch $FILE_NAME
echo "Start syslog generation. File name - $FILE_NAME"
for i in $(seq 1 $1)
do
	for j in $(seq 1 $COUNTPS)
	do
		SYSLOG_MSG=${MESSAGES[$RANDOM%${#MESSAGES[@]}]}
		PRIORITY=${PRIORITIES[ $RANDOM % ${#PRIORITIES[@]} ]}
		RANDOM_TIME=`date -d "$(( RANDOM%1+2010 ))-$(( RANDOM%12+1 ))-$(( RANDOM%28+1 )) $(( RANDOM%23+1 )):$(( RANDOM%59+1 )):$(( RANDOM%59+1 ))" '+%d-%m-%Y %H:%M:%S'`
		echo "$PRIORITY `env LANG=us_US.UTF-8 date "+%b %d $RANDOM_TIME"` 127.0.0.1 service: $SYSLOG_MSG" >> $FILE_NAME
	done	
done

echo "copy $FILE_NAME to hdfs. Directories /hw2/in and /hw2/out will be created"

hdfs dfs -mkdir -p /hw2/in
hdfs dfs -put $FILE_NAME /hw2/in
hdfs dfs -mkdir -p /hw2/out

if [ $? -ne 0 ]; then
	echo "There were errors."
	exit 1;
fi
echo "Done";