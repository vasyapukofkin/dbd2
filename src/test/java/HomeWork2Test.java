import hw2.HomeWork2;
import hw2.SyslogStat;
import static org.junit.Assert.*;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Irina Rumyntseva
 * Test for syslog aggregation application
 */
public class HomeWork2Test {
    private SparkSession sparkContext;
    private JavaSparkContext javaSparkContext;
    StructType schema;

    @Before
    public void setUp() {
        sparkContext = SparkSession.builder().master("local")
                .appName("hw2-test")
                .getOrCreate();
        javaSparkContext = new JavaSparkContext(sparkContext.sparkContext());
        List<StructField> schemaFields = new ArrayList<>();
        schemaFields.add(DataTypes.createStructField("priority", DataTypes.IntegerType, false));
        schemaFields.add(DataTypes.createStructField("hour", DataTypes.IntegerType, false));
        schema = DataTypes.createStructType(schemaFields);
    }

    /**
     * Test for aggregation by priority
     */
    @Test
    public void aggregateTest() {
        JavaRDD<Row> rdd = createSyslogRdd();
        Dataset<Row> dataFrame = sparkContext.createDataFrame(rdd, schema);
        dataFrame.createOrReplaceTempView("syslog");

        Dataset<Row> dataset = sparkContext.sql(HomeWork2.SQL_TEXT);
        Dataset<String> priorityList = dataset.map((MapFunction<Row, String>) s ->
                s.get(0) + " " + s.get(1) + " " + s.get(2), Encoders.STRING());
        List<String> list = priorityList.collectAsList();

        assertSyslogEquals(list);
    }

    /**
     * Test correct read from file
     */
    @Test
    public void aggregateFromFile() {
        JavaRDD<SyslogStat> rdd = readRddFromFile();
        Dataset<Row> dataFrame = sparkContext.createDataFrame(rdd, SyslogStat.class);
        dataFrame.createOrReplaceTempView("syslog");

        List<Row> actual = sparkContext.sql("select priority, hour from syslog").collectAsList();

        assertListEquals(actual);
    }

    private void assertListEquals(List<Row> actual) {
        List<Row> expected = new ArrayList<>();
        expected.add(RowFactory.create(5, 11));
        expected.add(RowFactory.create(7, 20));
        expected.add(RowFactory.create(4, 15));
        expected.add(RowFactory.create(1, 10));
        expected.add(RowFactory.create(3, 18));

        assertEquals(actual, expected);
    }

    private void assertSyslogEquals(List<String> list) {
        List<String> expected = new ArrayList<>();
        expected.add("1 12 2");
        expected.add("2 13 1");
        assertEquals(expected, list);
    }

    private JavaRDD<Row> createSyslogRdd() {
        List<SyslogStat> list = new ArrayList<>();
        list.add(new SyslogStat(1, 12, 0));
        list.add(new SyslogStat(1, 12, 0));
        list.add(new SyslogStat(2, 13, 0));
        return javaSparkContext.parallelize(list).map(l -> RowFactory.create(l.getPriority(), l.getHour()));
    }

    private JavaRDD<SyslogStat> readRddFromFile() {
        return sparkContext.read().textFile("src/test/resources/test.log")
                .javaRDD().map(s -> {
                    String[] values = s.split(" ");
                    SyslogStat syslogStat = new SyslogStat();
                    syslogStat.setPriority(Integer.parseInt(values[0].trim()));
                    String[] time = values[4].split(":");
                    syslogStat.setHour(Integer.parseInt(time[0]));
                    return syslogStat;
                });
    }

    @After
    public void after() {
        sparkContext.close();
    }
}
