package hw2;

import java.io.Serializable;

/**
 * @author Irina Rumyntseva
 * Main data class
 */
public class SyslogStat implements Serializable {
    private int priority;
    private int hour;
    private int count;

    public SyslogStat(){}

    public SyslogStat(int priority, int hour, int count) {
        this.priority = priority;
        this.hour = hour;
        this.count = count;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
