package hw2;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;

import java.util.List;

/**
 * @author Irina Rumyntseva
 * spark app for aggregating syslog by priority count by hours
 */
public class HomeWork2 {

    /**
     * Main SQL query
     */
    public static final String SQL_TEXT = "SELECT priority, hour, count(*) as count  FROM syslog GROUP BY priority, hour ORDER BY priority, hour";
    public static final String HOST = "localhost";
    public static final int PORT = 12121;

    public static void main(String[] args) {
        if(args.length == 0 || args.length > 1){
            System.out.println("Usage: enter one argument - path to file...");
            System.exit(1);
        }
        /**
         * Create Spark session
         */
        SparkSession sparkSession = SparkSession.builder()
                .appName("HomeWork2")
                .getOrCreate();

        /**
         * Read data from text file
         */
        JavaRDD<String> syslogStatRDD = sparkSession.read()
                .textFile(args[0])
                .javaRDD();

        /**
         * Map file lines
         */
        JavaRDD<SyslogStat> syslogRDD = syslogStatRDD.map(s -> {
            SyslogStat syslogStat = new SyslogStat();
            String[] values = s.split(" ");
            try{
                int priority = Integer.parseInt(values[0].trim());
                String[] time = values[4].split(":");
                int hour = Integer.parseInt(time[0]);

                syslogStat.setPriority(priority);
                syslogStat.setHour(hour);
            } catch (NumberFormatException e ) {
                System.err.println("Wrong syslog line: " + s);
                syslogStat = new SyslogStat();
            }
            return syslogStat;
        });

        /**
         * Prepare data frame and execute SQL
         */
        Dataset<Row> syslogDF = sparkSession.createDataFrame(syslogRDD, SyslogStat.class);
        syslogDF.createOrReplaceTempView("syslog");
        Dataset<Row> queryDF = sparkSession.sql(SQL_TEXT);

        /**
         * Prepare result for write in flume
         */
        Dataset<String> priorityList = queryDF.map((MapFunction<Row, String>)s ->
                s.get(0) + " " + s.get(1) + " " + s.get(2), Encoders.STRING());
        priorityList.show();
        FlumeClient client = new FlumeClient(HOST, PORT);
        List<String> list = priorityList.collectAsList();
        String sendData = String.join("\n", list);
        client.sendData(sendData);

        /**
         * Clean flume and stop application
         */
        client.clean();
        sparkSession.stop();
    }
}
